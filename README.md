Projekt stworzyłem za pomocą Angular CLI.

Instrukcja uruchomienia:

Na samym początku wchodzimy w zakładkę downloads i pobieramy plik Prj-A.zip.

1)   Instalajca nodeJS -> https://nodejs.org/en/

2)   Instalacja Angular CLI -> https://github.com/angular/angular-cli (npm install -g @angular/cli)

3)   Pobieramy dany folder i go rozpakowujemy.

4)   W konsoli cmd wchodzimy w dany folder, a następnie wpisujemy w konsoli "ng serve".

5)   Strona odpala się na adresie: localhost:4200/
